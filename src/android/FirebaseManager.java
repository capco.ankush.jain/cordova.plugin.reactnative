package cordova.plugin.reactnative;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class FirebaseManager extends ReactContextBaseJavaModule {
    private String LOG_TAG = "FirebaseManager";
    private ReactApplicationContext context;
    public FirebaseManager(ReactApplicationContext reactContext) {
        super(reactContext);
        context = reactContext;
    }
    @Override
    public String getName(){return LOG_TAG;}
    @ReactMethod
    public void setPath(String path) {
        final Intent intent = new Intent("navigation");
        Bundle b = new Bundle();
        b.putString("screenID",path);
        intent.putExtras(b);
        LocalBroadcastManager.getInstance(context).sendBroadcastSync(intent);
        Log.e(LOG_TAG,">>> setPath: "+path);
    }
    @ReactMethod
    public void setOnboardingResult(boolean result) {
        final Intent intent = new Intent("onboarding");
        Bundle b = new Bundle();
        b.putBoolean("result",result);
        intent.putExtras(b);
        LocalBroadcastManager.getInstance(context).sendBroadcastSync(intent);
        Log.e(LOG_TAG,">>> setOnboardingResult: "+result);
    }
}
