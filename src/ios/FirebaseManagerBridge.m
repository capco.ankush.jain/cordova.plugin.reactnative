#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
@interface RCT_EXTERN_MODULE(FirebaseManager,NSObject)
RCT_EXTERN_METHOD(setPath:(NSString*)path)
RCT_EXTERN_METHOD(setOnboardingResult:(NSString*)result)
@end
