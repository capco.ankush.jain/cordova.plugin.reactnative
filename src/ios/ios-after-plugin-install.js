module.exports = function(context) {
	console.log('Replacing AppDelegate ...')
	var fs = context.requireCordovaModule('fs')
	var path = context.requireCordovaModule('path')
	var newAppDelegateHeader = path.join(context.opts.projectRoot,'SCBL_Ionic_RN_Plugin_New/ReactNative/src/ios/AppDelegate.h')
	var newAppDelegateSource = path.join(context.opts.projectRoot,'SCBL_Ionic_RN_Plugin_New/ReactNative/src/ios/AppDelegate.m')
	var appName = "SCBLifeEasy-Dev"
	var env = process.env.MY_ENV; // contains the environment variable
	if(env == "UAT"){appName="SCBLifeEasy-Uat"}
	else if (env == "SIT"){appName="SCBLifeEasy-Sit"}
	else if (env == "PROD"){appName="SCBLifeEasy"}
	console.log('App Name'+appName);
	var oldAppDelegateHeader = path.join(context.opts.projectRoot,'platforms/ios/'+appName+'/Classes/AppDelegate.h')
	var oldAppDelegateSource = path.join(context.opts.projectRoot,'platforms/ios/'+appName+'/Classes/AppDelegate.m')
	fs.createReadStream(newAppDelegateHeader).pipe(fs.createWriteStream(oldAppDelegateHeader))
	fs.createReadStream(newAppDelegateSource).pipe(fs.createWriteStream(oldAppDelegateSource))
	console.log('Replacing AppDelegate done.')
	
	console.log('Replacing Bridging-Header.h ...')
	var newBridgingHeader = path.join(context.opts.projectRoot,'SCBL_Ionic_RN_Plugin_New/ReactNative/src/ios/Bridging-Header.h')
	var oldBridgingHeader = path.join(context.opts.projectRoot,'platforms/ios/'+appName+'/Bridging-Header.h')
	fs.createReadStream(newBridgingHeader).pipe(fs.createWriteStream(oldBridgingHeader))
	console.log('Replacing Bridging-Header.h done.')
}