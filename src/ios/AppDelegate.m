#import "AppDelegate.h"
#import "MainViewController.h"
#import "SCBLifeEasy_Dev-Swift.h"

@implementation AppDelegate
@synthesize activeLife;

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    [[HealthKitManager getSharedInstance] startObservingHealthKitStore];
    self.viewController = [[MainViewController alloc] init];
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

@end
