import Foundation
@objc(FirebaseManager)
public class FirebaseManager:NSObject {
    @objc(setPath:)
    func setPath(path:String)->Void {
        NotificationCenter.default.post(name:Notification.Name("navigation"),object:nil,userInfo:["screenID":path])
        print(">>> setPath: \(path)")
    }
    @objc(setOnboardingResult:)
    func setOnboardingResult(result:Bool)->Void {
        NotificationCenter.default.post(name:Notification.Name("onboarding"),object:nil,userInfo:["result":result])
        print(">>> setOnboardingResult: \(result)")
    }
}
